package fr.ia.springrestService.restService.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProgicaConnect
{

    /**
     * @author IOOSSEN AURELIEN created on 12/11/2019
     */

    private static Connection connection;
    private String userName = "java_user";
    private String password = "1234";
    private String url = "jdbc:sqlserver://STA7400562:1433;databaseName=PROGICA";


    private ProgicaConnect()
    {
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

            connection = DriverManager.getConnection(url, userName, password);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static Connection getInstance()
    {

        if (connection == null)
        {

            new ProgicaConnect();
        }
        return connection;
    }


}
