package fr.ia.springrestService.restService.dao;

import fr.ia.springrestService.restService.job.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author IOOSSEN AURELIEN created on 12/11/2019
 */

@Repository
public interface PersonneDAO extends JpaRepository<Personne, Integer>
{

    Personne findById(int id);

    List<Personne> findAll();
}
