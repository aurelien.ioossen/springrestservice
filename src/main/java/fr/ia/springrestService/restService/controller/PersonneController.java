package fr.ia.springrestService.restService.controller;


import fr.ia.springrestService.restService.dao.PersonneDAO;
import fr.ia.springrestService.restService.job.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonneController
{

    /**
     * @author IOOSSEN AURELIEN created on 12/11/2019
     */

    @Autowired
    private PersonneDAO personneDAO;

    @GetMapping(value = "personnes/{id}")
    public ResponseEntity getPersoneByid(@PathVariable int id)
    {


        Personne personne = personneDAO.findById(id);
        if (personne == null){
            return ResponseEntity.noContent().build();

        }
        return ResponseEntity.ok(personne);
    }


    @GetMapping(value = "personnes")
    public ResponseEntity getAllPersonnes(){

        List<Personne> listPersonne = personneDAO.findAll();
        if ( listPersonne != null){
            return ResponseEntity.ok(listPersonne) ;

        }else {
            return ResponseEntity.noContent().build();
        }


    }
}
